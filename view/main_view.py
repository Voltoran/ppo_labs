# Demo view
from tkinter import *
from tkinter import colorchooser as cc
from tkinter import filedialog as fd
from tkinter import ttk as ttk

import control.plugin_work as plw
from view.windows import *


class MainWindow:
    def __init__(self, cn):

        self.cn = cn
        self.window_counter = 0
        self.master = Tk()
        self.master.wm_title("Groups")
        self.master.minsize(width=900, height=500)
        self.master.maxsize(width=900, height=500)

        self.frame = Frame(self.master, width=900, height=500)

        self.tree = ttk.Treeview(self.frame)
        self.chg_color()

        # create tree structure
        self.tree["columns"] = ("one", "two", "tree")
        self.tree.column("one", width=200)
        self.tree.column("two", width=200)
        self.tree.column("tree", width=200)

        self.tree.heading("one", text="Name")
        self.tree.heading("two", text="Second name")
        self.tree.heading("tree", text="Surname")

        # Menu's
        # PopUp menus
        # create a menu root
        self.popup_3 = Menu(self.master, tearoff=0)
        self.popup_3.add_command(label="Add group", command=lambda: self.new_window(AddGroupWindow))

        # create a menu student
        self.popup_1 = Menu(self.master, tearoff=0)
        self.popup_1.add_command(label="Delete", command=lambda: self.new_window(DeleteStudentWindow))

        # create a menu group
        self.popup_2 = Menu(self.master, tearoff=0)
        self.popup_2.add_command(label="Add student", command=lambda: self.new_window(AddStudentWindow))
        self.popup_2.add_command(label="Delete", command=lambda: self.new_window(DeleteGroupWindow))
        self.popup_2.add_command(label="Rename", command=lambda: self.new_window(RenameGroupWindow))

        plw.load_plugins(self)

        # Upper menu
        menubar = Menu(self.frame)
        filemenu = Menu(menubar, tearoff=0)
        commandmenu = Menu(menubar, tearoff=0)
        settings = Menu(menubar, tearoff=0)

        menubar.add_cascade(label="File", menu=filemenu)
        menubar.add_cascade(label="Commands", menu=commandmenu)
        menubar.add_cascade(label="Settings", menu=settings)

        filemenu.add_command(label="Open", command=self.load)
        filemenu.add_command(label="Save", command=self.save)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.master.destroy)

        commandmenu.add_command(label="Undo", command=self.undo)
        commandmenu.add_command(label="Redo", command=self.redo)

        settings.add_command(label="Change table color", command=self.set_color)

        self.master.config(menu=menubar)

        # Binds
        self.tree.bind("<Button-2>", self.do_popup)
        self.tree.bind("<Double-Button-1>", self.show_card)

        self.tree.pack(fill=tk.BOTH, expand=1)
        self.frame.pack(fill=tk.BOTH, expand=1)

        self.master.mainloop()

    # Methods
    def do_popup(self, event):

        g_set = self.tree.tag_has('g')
        s_set = self.tree.tag_has('s')
        try:
            g_set.index(self.tree.selection()[0])
            self.popup_2.post(event.x_root, event.y_root)
        except ValueError:
            try:
                s_set.index(self.tree.selection()[0])
                self.popup_1.post(event.x_root, event.y_root)
            except ValueError:
                self.popup_3.post(event.x_root, event.y_root)


    def set_color(self):
        color = cc.askcolor()
        d = dict(table_color=color[1])
        self.cn.change_settings(d)
        self.chg_color()

    def chg_color(self):
        ttk.Style().configure("Treeview", background=self.cn.settings["table_color"],
                              fieldbackground=self.cn.settings["table_color"])

    def load(self):
        path = fd.askopenfilename(initialdir=self.cn.BASE_DIR,
                                  filetypes=(("Text File", "*.json"), ("All Files", "*.*")),
                                  title="Choose a file.")
        if path != "":
            self.cn.load_file(path)

            data = self.cn.obj_to_strings()
            vf.clear_rows(self.tree)
            vf.insert_rows(data, self.tree)

    def save(self):
        path = fd.asksaveasfilename(initialdir=self.cn.BASE_DIR,
                                    filetypes=(("Text File", "*.json"), ("All Files", "*.*")),
                                    title="Choose a file.")

        if path != "":
            self.cn.save_file(path)

            data = self.cn.obj_to_strings()
            vf.clear_rows(self.tree)
            vf.insert_rows(data, self.tree)

    def undo(self):
        self.cn.undo()

        data = self.cn.obj_to_strings()
        vf.clear_rows(self.tree)
        vf.insert_rows(data, self.tree)

    def redo(self):
        self.cn.redo()

        data = self.cn.obj_to_strings()
        vf.clear_rows(self.tree)
        vf.insert_rows(data, self.tree)

    def add_row(self, menu_key, row_name, action):
        d = dict(root_menu=self.popup_3,
                 student_menu=self.popup_1,
                 group_menu=self.popup_2)

        d[menu_key].add_command(label=row_name, command=action)

    def show_card(self, event):
        if self.tree.tag_has('g', self.tree.selection()):
            self.new_window(ShowGroupInfoWindow)
        if self.tree.tag_has('s', self.tree.selection()):
            self.new_window(ChangeStudentInfoWindow)

    def new_window(self, win_class):
        if self.window_counter != 0:
            self.window_counter = 0
            self.newWindow.destroy()

        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.wm_title("Dialog")
        self.window_counter += 1

        d = dict(master=self.newWindow,
                 container_obj=self.cn,
                 selected_item_parent_id=self.tree.index(self.tree.parent(self.tree.selection())),
                 selected_item_id=self.tree.index(self.tree.selection()),
                 tree=self.tree)

        app = win_class(d)
