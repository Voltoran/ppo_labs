import tkinter as tk
from tkinter import messagebox as ms
import view.view_functions as vf


class Window:

    def close(self):
        pass


# Rename group
class RenameGroupWindow(Window):
    def __init__(self, data_dict):
        name = "Rename group"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        self.ent1 = tk.Entry(data_dict['master'], width=20, bd=3)
        self.ent1.pack()
        self.Button = tk.Button(self.frame, text='OK', width=25,
                                command=lambda: self.chg_name(data_dict['container_obj']
                                                              , data_dict['tree']))
        self.Button.pack()
        self.frame.pack()

    def chg_name(self, container_obj, tree):
        if self.ent1.get() == "":
            ms.showinfo("Warning",  "Need to fill name!")
        else:
            res = container_obj.rename_group(self.group_id, self.ent1.get())

            if res == ValueError:
                ms.showinfo("Warning", "Group name is already exist!")
            else:
                data = container_obj.obj_to_strings()
                vf.clear_rows(tree)
                vf.insert_rows(data, tree)
                self.close()


    def close(self):
        self.master.destroy()


# show group info
class ShowGroupInfoWindow(Window):
    def __init__(self, data_dict):
        name = "Group information"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        data = data_dict['container_obj'].show_group_info(self.group_id)
        l1 = tk.Label(self.master, text="№: " + data[0])
        l1.pack(fill=tk.X)
        l2 = tk.Label(self.master, text="Name: " + data[1])
        l2.pack(fill=tk.X)
        l3 = tk.Label(self.master, text="Number of students: " + data[2])
        l3.pack(fill=tk.X)
        l4 = tk.Label(self.master, text="Max rating: " + data[3])
        l4.pack(fill=tk.X)
        l5 = tk.Label(self.master, text="Min rating: " + data[4])
        l5.pack(fill=tk.X)
        l6 = tk.Label(self.master, text="Avr rating: " + data[5])
        l6.pack(fill=tk.X)
        self.Button = tk.Button(self.frame, text='OK', width=25, command=self.close)
        self.Button.pack()
        self.frame.pack()

    def close(self):
        self.master.destroy()


# Change student
class ChangeStudentInfoWindow(Window):
    def __init__(self, data_dict):
        name = "Student information"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_parent_id']
        self.student_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        self.data = data_dict['container_obj'].show_student_info(self.group_id, self.student_id)
        l1 = tk.Label(self.master, text="Surname: " + self.data[0])
        l1.pack()
        l2 = tk.Label(self.master, text="Name: " + self.data[1])
        l2.pack()
        l3 = tk.Label(self.master, text="Second name: " + self.data[2])
        l3.pack()
        l4 = tk.Label(self.master, text="Rating: " + self.data[3])
        l4.pack()
        l5 = tk.Label(self.master, text="Group name: " + self.data[4])
        l5.pack()
        l6 = tk.Label(self.master, text="Role: " + self.data[5])
        l6.pack()
        self.Button1 = tk.Button(self.frame, text='OK', width=25, command=self.close)
        self.Button1.pack()
        self.frame.pack()

        self.frame_2 = tk.Frame(self.master, height=2, bd=1)
        l1 = tk.Label(self.frame_2, text="Change information")
        l1.pack()
        self.ent1 = tk.Entry(self.frame_2, width=20, bd=3)
        self.ent1.insert(tk.END, self.data[0])
        self.ent1.pack()

        self.ent2 = tk.Entry(self.frame_2, width=20, bd=3)
        self.ent2.insert(tk.END, self.data[1])
        self.ent2.pack()

        self.ent3 = tk.Entry(self.frame_2, width=20, bd=3)
        self.ent3.insert(tk.END, self.data[2])
        self.ent3.pack()

        self.ent4 = tk.Entry(self.frame_2, width=20, bd=3)
        self.ent4.insert(tk.END, self.data[3])
        self.ent4.pack()

        self.ent6 = tk.Entry(self.frame_2, width=20, bd=3)
        self.ent6.insert(tk.END, self.data[5])
        self.ent6.pack()

        self.Button2 = tk.Button(self.frame_2, text='Confirm', width=25,
                                 command=lambda: self.change_student_info(data_dict['container_obj'],
                                                                          data_dict['tree']))
        self.Button2.pack()
        self.Button3 = tk.Button(self.frame_2, text='Cancel', width=25, command=self.close)
        self.Button3.pack()
        self.frame_2.pack()

    def change_student_info(self, container_obj, tree):
        container_obj.change_student_info(self.group_id, self.student_id, self.ent1.get(), self.ent2.get(),
                                          self.ent3.get(), self.ent4.get(), self.data[4], self.ent6.get())
        data = container_obj.obj_to_strings()
        vf.clear_rows(tree)
        vf.insert_rows(data, tree)
        self.close()

    def close(self):
        self.master.destroy()


# Add student
class AddStudentWindow(Window):
    def __init__(self, data_dict):
        name = "Add student"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame_2 = tk.Frame(self.master, height=2, bd=1)
        l1 = tk.Label(self.frame_2, text="Input information")
        l1.pack()

        l2 = tk.Label(self.frame_2, text="Surname")
        self.ent1 = tk.Entry(self.frame_2, width=20, bd=3)
        l2.pack()
        self.ent1.pack()

        l3 = tk.Label(self.frame_2, text="Name")
        self.ent2 = tk.Entry(self.frame_2, width=20, bd=3)
        l3.pack()
        self.ent2.pack()

        l4 = tk.Label(self.frame_2, text="Second name")
        self.ent3 = tk.Entry(self.frame_2, width=20, bd=3)
        l4.pack()
        self.ent3.pack()

        l5 = tk.Label(self.frame_2, text="Rating")
        self.ent4 = tk.Entry(self.frame_2, width=20, bd=3)
        l5.pack()
        self.ent4.pack()

        l6 = tk.Label(self.frame_2, text="Role")
        self.ent6 = tk.Entry(self.frame_2, width=20, bd=3)
        l6.pack()
        self.ent6.pack()

        self.Button2 = tk.Button(self.frame_2, text='Confirm', width=25,
                                 command=lambda: self.add_student_info(data_dict['container_obj'], data_dict['tree']))
        self.Button2.pack()
        self.Button3 = tk.Button(self.frame_2, text='Cancel', width=25, command=self.close)
        self.Button3.pack()
        self.frame_2.pack()

    def add_student_info(self, container_obj, tree):
        container_obj.add_student(self.group_id, self.ent1.get(),
                                  self.ent2.get(),
                                  self.ent3.get(), self.ent4.get(),
                                  container_obj.group_container[self.group_id].name, self.ent6.get())

        data = container_obj.obj_to_strings()
        vf.clear_rows(tree)
        vf.insert_rows(data, tree)
        self.close()

    def close(self):
        self.master.destroy()


# Add group
class AddGroupWindow(Window):
    def __init__(self, data_dict):
        name = "Add group"
        data_dict['master'].wm_title(name)

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        self.ent1 = tk.Entry(self.frame, width=20, bd=3)
        self.ent1.pack()
        self.Button = tk.Button(self.frame, text='OK', width=25,
                                command=lambda: self.add_group(data_dict['container_obj'], data_dict['tree']))
        self.Button.pack()
        self.frame.pack()

    def add_group(self, container_obj, tree):
        if self.ent1.get() == "":
            ms.showinfo("Warning",  "Need to fill name!")
        else:
            res = container_obj.add_group(self.ent1.get())
            if res == ValueError:
                ms.showinfo("Warning", "Group тame is already exist!")
            else:
                container_obj.add_student(len(container_obj.group_container.group_list) - 1, "empty",
                                      "empty", "empty", 0, self.ent1.get(), "empty")
                data = container_obj.obj_to_strings()
                vf.clear_rows(tree)
                vf.insert_rows(data, tree)
                self.close()



    def close(self):
        self.master.destroy()


# Delete group
class DeleteGroupWindow(Window):
    def __init__(self, data_dict):
        name = "Delete group"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        l1 = tk.Label(self.frame, text="You are sure you want to delete the group " +
                                       data_dict['container_obj'].group_container.group_list[self.group_id].name + "?")
        l1.pack()
        self.Button1 = tk.Button(self.frame, text='Yes', width=25,
                                 command=lambda: self.delete_group(data_dict['container_obj'], data_dict['tree']))
        self.Button1.pack()
        self.Button2 = tk.Button(self.frame, text='No', width=25, command=self.close)
        self.Button2.pack()
        self.frame.pack()

    def delete_group(self, container_obj, tree):
        container_obj.delete_group(self.group_id)

        data = container_obj.obj_to_strings()
        vf.clear_rows(tree)
        vf.insert_rows(data, tree)
        self.master.destroy()

    def close(self):
        self.master.destroy()


# Delete student
class DeleteStudentWindow(Window):
    def __init__(self, data_dict):
        name = "Delete student"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_parent_id']
        self.student_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        l1 = tk.Label(self.frame, text="You are sure you want to delete the student " +
                                       data_dict['container_obj'].group_container.group_list[self.group_id].stud_list[
                                           self.student_id].name + "?")
        l1.pack()

        self.Button1 = tk.Button(self.frame, text='Yes', width=25,
                                 command=lambda: self.delete_student(data_dict['container_obj'], data_dict['tree']))
        self.Button1.pack()
        self.Button2 = tk.Button(self.frame, text='No', width=25, command=self.close)
        self.Button2.pack()
        self.frame.pack()

    def delete_student(self, container_obj, tree):
        container_obj.delete_student(self.group_id, self.student_id)

        data = container_obj.obj_to_strings()
        vf.clear_rows(tree)
        vf.insert_rows(data, tree)
        self.master.destroy()

    def close(self):
        self.master.destroy()
