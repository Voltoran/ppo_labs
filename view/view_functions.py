def insert_rows(data, tree):

    group_name_list = []
    index_list = []
    count = 1

    for i in range(len(data)):
        try:
            gn = data[i]["Group"]
            id = group_name_list.index(gn)
        except ValueError:
            group_name_list.append(gn)
            index_list.append(0)
            id = len(group_name_list) - 1
            tree.insert("", id, gn, text=str(count) + ") " + gn, tags='g', open=True)
            count += 1

        tree.insert(gn, index_list[id], text=str(index_list[id] + 1), values=(data[i]["Name"],
                                                                              data[i]["SecondName"],
                                                                              data[i]["Surname"]), tags='s')
        index_list[id] += 1


def clear_rows(tree):
    for i in tree.get_children():
        tree.delete(i)

