from control import control as ctrl
from view import main_view as mv


def main():
    gkd = ctrl.Control()
    mv.MainWindow(gkd)


if __name__ == "__main__":
    main()
