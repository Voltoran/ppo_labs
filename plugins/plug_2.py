import tkinter as tk

import control.plugin_work as pl


# Rename group
class PluginWindowGroup:
    def __init__(self, data_dict):
        name = "Plugin 2 window"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)

        student_list = data_dict['container_obj'].group_container.group_list[self.group_id].stud_list
        max_rating = max(list(map(lambda x: x.rating, student_list)))
        rating_list = list(map(lambda x: x.rating, student_list))

        index = rating_list.index(max_rating)

        message = "Best student is " + student_list[index].name + " " + student_list[index].surname + "!"

        l1 = tk.Label(self.master, text=message)
        l1.pack()
        self.Button = tk.Button(self.frame, text='OK', width=25, command=self.close)
        self.Button.pack()
        self.frame.pack()

    def close(self):
        self.master.destroy()


class ReportPlugin(pl.Plugin):
    name = 'Find best plugin (2)'

    # Методы обратной связи
    def on_load(self, obj_view):
        print(self.name + " loaded!")
        obj_view.add_row("group_menu", "Find best(plugin 2)", lambda: obj_view.new_window(PluginWindowGroup))
