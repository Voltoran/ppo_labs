import tkinter as tk

import control.plugin_work as pl


# Rename group
class PluginWindowGroup:
    def __init__(self, data_dict):
        name = "Plugin 1 window"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        data = data_dict['container_obj'].show_group_info(self.group_id)

        rating = int(data[5])
        message = ""

        if rating < 61:
            message = "Bad"

        if 61 <= rating < 71:
            message = "Not bad"

        if 71 <= rating < 85:
            message = "Good"

        if rating > 85:
            message = "Excellent"

        l1 = tk.Label(self.master, text=message + " group, with average rating: " + data[5])
        l1.pack()
        self.Button = tk.Button(self.frame, text='OK', width=25, command=self.close)
        self.Button.pack()
        self.frame.pack()

    def close(self):
        self.master.destroy()


class PluginWindowStudent:
    def __init__(self, data_dict):
        name = "Plugin 1 window"
        data_dict['master'].wm_title(name)

        self.group_id = data_dict['selected_item_parent_id']
        self.student_id = data_dict['selected_item_id']

        self.master = data_dict['master']
        self.frame = tk.Frame(self.master)
        data = data_dict['container_obj'].show_student_info(self.group_id, self.student_id)

        rating = int(data[3])
        message = ""

        if rating < 61:
            message = "Bad student"

        if 61 <= rating < 71:
            message = "Not bad student"

        if 71 <= rating < 85:
            message = "Good student"

        if rating >= 85:
            message = "Excellent student"

        l1 = tk.Label(self.master, text=message + ", with rating: " + data[3])
        l1.pack()
        self.Button = tk.Button(self.frame, text='OK', width=25, command=self.close)
        self.Button.pack()
        self.frame.pack()

    def close(self):
        self.master.destroy()


class ReportPlugin(pl.Plugin):
    name = 'Report plugin (1)'

    # Методы обратной связи
    def on_load(self, obj_view):
        print(self.name + " loaded!")
        obj_view.add_row("student_menu", "Report(plugin 1)", lambda: obj_view.new_window(PluginWindowStudent))
        obj_view.add_row("group_menu", "Report(plugin 1)", lambda: obj_view.new_window(PluginWindowGroup))
