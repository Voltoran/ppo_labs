from model import base_classes as mc
from model import json_processing as jsw
from model import command as cmd
import os


class Control:
    def __init__(self):
        self.BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        self.group_container = mc.GroupContainer()
        self.student_data = list()
        self.group_data = list()
        self.settings = dict(table_color="white")
        self.load_settings()

    def load_settings(self):
        try:
            settings_data = jsw.WorkWithJson.load(self.BASE_DIR + "/settings.json")
            self.settings = settings_data[0]
        except TypeError:
            pass

    def save_settings(self):
        jsw.WorkWithJson.save(self.BASE_DIR + "/settings.json", [self.settings])

    def change_settings(self, new_set):
        self.settings = new_set
        self.save_settings()

    # Commands methods
    def obj_to_strings(self):
        return jsw.MakeJson.create_dict_list(jsw.MakeJson, self.group_container)

    def load_file(self, path):
        print("load file")
        self.group_container = cmd.LoadFileCommand.execute(path)

    def save_file(self, path):
        print("save file")

        cmd.SaveFileCommand.execute(self.group_container, path)

    def add_group(self, name):
        command = cmd.AddGroupCommand()
        command.group_container = self.group_container
        command.group_name = name
        exeption = cmd.exe_command(command)

        print("add group" + str(name))
        return exeption

    def delete_group(self, group_id):
        command = cmd.DeleteGroupCommand()
        command.group_container = self.group_container
        command.group_id = group_id
        cmd.exe_command(command)

        print("delete group" + str(group_id))

    def rename_group(self, group_id, name):
        command = cmd.RenameGroup()
        command.group_container = self.group_container
        command.group_id = group_id
        command.group_name = name
        exeption = cmd.exe_command(command)

        print("rename group " + str(group_id) + " to " + name)

        return exeption

    def show_group_info(self, group_id):
        command = cmd.ShowGroupInfo()
        command.group_container = self.group_container
        command.group_id = group_id
        cmd.exe_command(command)

        print("show group info")
        return command.data

    def add_student(self, group_id, surname, name, second_name, rating, group_name, role):
        command = cmd.AddStudentCommand()
        command.group_container = self.group_container
        command.group_id = group_id
        command.student = mc.Student(surname, name, second_name, rating, group_name, role)
        cmd.exe_command(command)

        print("add student")

    def delete_student(self, group_id, student_id):
        command = cmd.DeleteStudentCommand()
        command.group_container = self.group_container
        command.group_id = group_id
        command.student_id = student_id
        cmd.exe_command(command)

        print("delete student")

    def change_student_info(self, group_id, student_id, surname, name, second_name, rating, group_name, role):
        command = cmd.ChangeStudentInfo()
        command.group_container = self.group_container
        command.group_id = group_id
        command.student_id = student_id
        command.student = mc.Student(surname, name, second_name, rating, group_name, role)
        cmd.exe_command(command)

        print("change student info")

    def show_student_info(self, group_id, student_id):
        command = cmd.ShowStudentInfo()
        command.group_container = self.group_container
        command.group_id = group_id
        command.student_id = student_id
        cmd.exe_command(command)

        print("show student info")
        return command.data

    def redo(self):
        command = cmd.RedoCommand()
        cmd.exe_command(command)
        print("redo command")

    def undo(self):
        command = cmd.UndoCommand()
        cmd.exe_command(command)
        print("undo command")
