from sys import stdout as console
import model.base_classes as mc
import model.json_processing as jsw


# Commands for files

class LoadFileCommand:
    @staticmethod
    def execute(path):
        raw_data = jsw.WorkWithJson.load(path)
        return jsw.ParseJson.create_group_container(jsw.ParseJson, raw_data)


class SaveFileCommand:
    @staticmethod
    def execute(group_container, path):
        dict_list = jsw.MakeJson.create_dict_list(jsw.MakeJson, group_container)
        jsw.WorkWithJson.save(path, dict_list)


# Show commands
class ShowGroupInfo:
    def __init__(self):
        self.group_container = mc.GroupContainer()
        self.group_id = 0
        self.data = list()

    def execute(self):
        self.data.append(str(self.group_id + 1))
        self.data.append(str(self.group_container.group_list[self.group_id].name))
        self.data.append(str(len(self.group_container.group_list[self.group_id].stud_list)))
        self.data.append(str(self.group_container.group_list[self.group_id].get_max_rating()))
        self.data.append(str(self.group_container.group_list[self.group_id].get_min_rating()))
        self.data.append(str(self.group_container.group_list[self.group_id].get_avr_rating()))


class ShowStudentInfo:
    def __init__(self):
        self.data = list()
        self.group_container = mc.GroupContainer()
        self.group_id = 0
        self.student_id = 0

    def execute(self):
        self.data.append(str(self.group_container.group_list[self.group_id].stud_list[self.student_id].surname))
        self.data.append(str(self.group_container.group_list[self.group_id].stud_list[self.student_id].name))
        self.data.append(str(self.group_container.group_list[self.group_id].stud_list[self.student_id].second_name))
        self.data.append(str(self.group_container.group_list[self.group_id].stud_list[self.student_id].rating))
        self.data.append(str(self.group_container.group_list[self.group_id].name))
        self.data.append(str(self.group_container.group_list[self.group_id].stud_list[self.student_id].role))


# Command that CHANGE interface
class Command:
    def execute(self):
        raise NotImplementedError()

    def cancel(self):
        raise NotImplementedError()

    def name(self):
        raise NotImplementedError()


# Group commands
class DeleteGroupCommand(Command):
    def __init__(self):
        self.group_container = mc.GroupContainer()
        self.group_id = 0
        self.__group = mc.Group("default")

    def execute(self):
        if len(self.group_container.group_list) != 0:
            self.__group = self.group_container.group_list[self.group_id]
            self.group_container.__delitem__(self.group_id)
        else:
            raise IndexError

    def cancel(self):
        self.group_container.group_list.insert(self.group_id, self.__group)

    def name(self):
        return "DeleteGroupCommand"


class AddGroupCommand(Command):
    def __init__(self):
        self.group_container = mc.GroupContainer()
        self.group_name = ""
        self.names = list()

    def execute(self):
        for i in range(len(self.group_container.group_list)):
            self.names.append(self.group_container.group_list[i].name)

        try:
            self.names.index(self.group_name)
        except ValueError:
            self.group_container.__add__(mc.Group(self.group_name))
            return

        return ValueError

    def cancel(self):
        self.group_container.__delitem__(len(self.group_container.group_list) - 1)

    def name(self):
        return "AddGroupCommand"


class RenameGroup(Command):
    def __init__(self):
        self.group_container = mc.GroupContainer()
        self.group_name = ""
        self.group_id = 0
        self.__old_name = ""
        self.names = list()

    def execute(self):
        for i in range(len(self.group_container.group_list)):
            self.names.append(self.group_container.group_list[i].name)
        try:
            self.names.index(self.group_name)
        except ValueError:
            self.__old_name = self.group_container.group_list[self.group_id].name
            self.group_container.group_list[self.group_id].rename(self.group_name)
            return

        return ValueError


    def cancel(self):
        self.group_container.group_list[self.group_id].rename(self.__old_name)

    def name(self):
        return "RenameGroup"


# Student commands
class DeleteStudentCommand(Command):
    def __init__(self):
        self.group_container = mc.GroupContainer()
        self.group_id = 0
        self.student_id = 0
        self.__student = mc.Student("default", "default", "default", 0, "default", "default")

    def execute(self):
        self.__student = self.group_container.group_list[self.group_id].stud_list[self.student_id]
        self.group_container.group_list[self.group_id].__delitem__(self.student_id)

    def cancel(self):
        self.group_container.group_list[self.group_id].stud_list.insert(self.student_id, self.__student)

    def name(self):
        return "DeleteStudentCommand"


class AddStudentCommand(Command):
    def __init__(self):
        self.group_container = mc.GroupContainer()
        self.student = mc.Student("default", "default", "default", 0, "default", "default")
        self.group_id = 0

    def execute(self):
        self.group_container.group_list[self.group_id].__add__(self.student)

    def cancel(self):
        l = len(self.group_container.group_list[self.group_id].stud_list) - 1
        self.group_container.group_list[self.group_id].__delitem__(l)

    def name(self):
        return "upAddStudentCommandtime"


class ChangeStudentInfo(Command):
    def __init__(self):
        self.group_container = mc.GroupContainer()
        self.student = mc.Student("default", "default", "default", 0, "default", "default")
        self.group_id = 0
        self.student_id = 0
        self.__old_student = mc.Student("default", "default", "default", 0, "default", "default")

    def execute(self):
        self.__old_student = self.group_container.group_list[self.group_id].stud_list[self.student_id]
        self.group_container.group_list[self.group_id].reset_student(self.student_id, self.student)

    def cancel(self):
        self.group_container.group_list[self.group_id].reset_student(self.student_id, self.__old_student)

    def name(self):
        return "ChangeStudentInfo"


# Undo and redo commands
class UndoCommand:
    def execute(self):
        try:
            cmd = HISTORY.pop()
            TRASH.append(cmd)
            cmd.cancel()

        except IndexError:
            console.write("ERROR: HISTORY is empty\n")

    def name(self):
        return "UndoCommand"


# Команда redo
class RedoCommand:
    def execute(self):
        try:
            cmd = TRASH.pop()
            HISTORY.append(cmd)
            cmd.execute()

        except IndexError:
            console.write("ERROR: TRASH is empty\n")

    def name(self):
        return "RedoCommand"


HISTORY = list()
TRASH = list()


def exe_command(command):
    try:
        result = command.execute()

        if not isinstance(command, UndoCommand) and not isinstance(command, RedoCommand) \
                and not isinstance(command, ShowGroupInfo) and not isinstance(command, ShowStudentInfo):
            HISTORY.append(command)
            TRASH.clear()

        return result

    except Exception:
        print("Command don't run!")
