import json
from model import base_classes as mc


class WorkWithJson:
    @staticmethod
    def load(path):
        try:
            with open(path, 'r', encoding='utf-8') as data_file:
                raw_data = json.load(data_file)
            return raw_data
        except FileNotFoundError:
            print("File not found!")
        except ValueError:
            print("No data in file!")

    @staticmethod
    def save(path, raw_data):
        try:
            with open(path, 'w', encoding='utf-8') as outfile:
                json.dump(raw_data, outfile, indent=1)
        except FileNotFoundError:
            print("File not found!")
        except ValueError:
            print("No data in file!")


class ParseJson:
    def __create_student_list(self, raw_data):
        student_list = list()
        for i in range(len(raw_data)):
            temp = mc.Student(raw_data[i]['Surname'], raw_data[i]['Name'],
                              raw_data[i]['SecondName'], raw_data[i]['Rating'],
                              raw_data[i]['Group'], raw_data[i]['Role'])

            student_list.append(temp)
        return student_list

    @staticmethod
    def create_group_container(self, raw_data):
        data = mc.GroupContainer()
        group_name_list = []
        self.id_group = 0

        student_list = self.__create_student_list(self, raw_data)

        for i in range(len(student_list)):
            try:
                gn = student_list[i].group_name
                self.id_group = group_name_list.index(gn)
            except ValueError:
                group_name_list.append(gn)
                data.__add__(mc.Group(gn))
                self.id_group = len(group_name_list) - 1

            data.group_list[self.id_group].__add__(student_list[i])

        return data


class MakeJson:
    def __create_students_list(self, data):
        student_list = list()
        for i in range(len(data.group_list)):
            student_list += data.group_list[i].stud_list
        return student_list

    @staticmethod
    def create_dict_list(self, data):
        student_list = self.__create_students_list(self, data)
        dict_list = []

        for i in range(len(student_list)):
            s = student_list[i]

            d = dict(Surname=s.surname, Name=s.name,
                     SecondName=s.second_name, Rating=s.rating, Group=s.group_name, Role=s.role)

            dict_list.append(d)

        return dict_list

# Test
# raw_data = WorkWithJson.load("/Users/mvoroshnin/Desktop/lol/BMSTU/6_semestr/PPO/Lab_1/PPO_lab_1/groups.json")
# data = ParseJson.create_group_container(ParseJson, raw_data)
#
# dict_list = MakeJson.create_dict_list(MakeJson, data)
# WorkWithJson.save("/Users/mvoroshnin/Desktop/lol/BMSTU/6_semestr/PPO/Lab_1/PPO_lab_1/group_2.json", dict_list)
