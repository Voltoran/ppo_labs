import re

class Student:
    def __init__(self, surname, name, second_name, rating, group_name, role):
        self.surname = surname
        self.name = name
        self.second_name = second_name
        try:
            self.rating = int(rating)
            if self.rating > 100 or self.rating < 0:
                self.rating = 0
        except ValueError:
            self.rating = 0

        self.group_name = group_name
        self.role = role


class Group:
    # Fields
    head_id = 0
    name = "Group ?"

    # Methods
    def __init__(self, name):
        self.name = name
        self.stud_list = list()

    def __add__(self, student):
        if re.match(r'[H, h][E, e][A, a][D, d]', student.role):
            student.role = "head"
            self.stud_list.append(student)
            self.reset_head(len(self.stud_list) - 1)
        else:
            student.role = "student"
            self.stud_list.append(student)

    def __delitem__(self, stud_id):
        del self.stud_list[stud_id]

    def rename(self, new_name):
        self.name = new_name
        for i in range(len(self.stud_list)):
            self.stud_list[i].group_name = self.name

    def reset_head(self, id):
        self.stud_list[self.head_id].role = "student"
        self.head_id = id

    def reset_student(self, stud_id, student):
        if student.role == "head":
            self.reset_head(stud_id)
        self.stud_list[stud_id] = student

    def get_max_rating(self):
        return max(list(map(lambda x: x.rating, self.stud_list)))

    def get_min_rating(self):
        return min(list(map(lambda x: x.rating, self.stud_list)))

    def get_avr_rating(self):
        l = (list(map(lambda x: x.rating, self.stud_list)))
        return int(sum(l) / float(len(l)))


class GroupContainer:
    # Fields
    def __init__(self):
        self.group_list = list()

    def __add__(self, new_group):
        self.group_list.append(new_group)

    def __delitem__(self, group_id):
        del self.group_list[group_id]

    def __getitem__(self, id):
        return self.group_list[id]
